<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Request;
use \App\Article;


class PageController extends Controller
{
    public function index()
    {

        $articles = Article::latest()->get();
        return view('index')->with('articles', $articles);
    }

    public function article($id)
    {
        $article = Article::find($id);
        return view('post.article')->with('article', $article);

    }

    public function create()
    {
        return view('post.post');
    }

    public function store()
    {
        $input = Request::all();

        $input['created_at'] = Carbon::now();
        $input['author'] = 'Admin';
        $subinput = substr($input['description'], 0, 700);
        $input['intro'] =  $subinput;

        Article::create($input);
        return redirect('/');
    }

}

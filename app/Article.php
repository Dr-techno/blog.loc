<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'description',
        'author',
        'intro',
        'updated_at',
        'created_at',
    ];

    public function getTitleAttribute($value)
    {
        return $value;
    }

    public function setIntroAttribute($value)
    {
        $this->attributes['intro'] = substr($this->attributes['description'], 0, 700);;
    }
}


@extends('layout.body')
@section('Articles')
    {!! Form::open(['url' => '/article']) !!}
    <div class="form-group">
        {!! Form::label('title','Title:') !!}
        {!! Form::text('title', '',['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description','Body:') !!}
        {!! Form::textarea('description', '',['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Add Article',['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@endsection
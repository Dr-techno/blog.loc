@extends('layout.body')
@section('Articles')
    @foreach($articles AS $article)
        <h2><a href="/article/{{$article['id']}}"> {{$article['title']}}</a></h2>
        <div>{{$article['intro']}} </div>

    @endforeach
@endsection